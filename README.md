# Docker compose para ejecutar Kong EE

* Iniciar sesión en el registry

    `docker login -u [USER] -p [PASSWORD] kong-docker-kong-enterprise-edition-docker.bintray.io`

* Pullear la imagen

    `docker pull kong-docker-kong-enterprise-edition-docker.bintray.io/kong-enterprise-edition`

* Taggear la imagen

    `docker tag kong-docker-kong-enterprise-edition-docker.bintray.io/kong-enterprise-edition kong-ee`

* Eliminar imagen original (opcional)

    `docker image rm kong-docker-kong-enterprise-edition-docker.bintray.io/kong-enterprise-edition`

* Obtener el json con la licencia y renombrarla a `license.json`

    * **Nota**: Este repositorio incluye una licencia para la POC de Sura.

* Ejecutar `docker-compose up -d`

* Probar la API con curl y obtenemos resultado del upstream:

    `curl -i -X POST --url http://localhost:8001/apis/ --data 'name=test' --data 'uris=/' --data 'upstream_url=http://httpbin.org/get'`